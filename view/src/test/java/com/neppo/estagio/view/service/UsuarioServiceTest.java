package com.neppo.estagio.view.service;


import com.neppo.estagio.core.models.Usuario;
import com.neppo.estagio.core.repositories.UsuarioRepository;
import com.neppo.estagio.view.services.UsuarioService;
import com.neppo.estagio.view.services.implementation.UsuarioServiceImpl;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.mockito.Mockito.*;
import static org.junit.Assert.*;

public class UsuarioServiceTest {

    private MockMvc mockMvc;

    @Mock
    private UsuarioRepository usuarioRepository;

    private UsuarioService usuarioService;

    private Usuario usuario;
    private List<Usuario> usuarios;

    @Before
    public void init(){
        MockitoAnnotations.initMocks(this);

        this.usuarioService = new UsuarioServiceImpl(this.usuarioRepository);

        this.mockMvc = MockMvcBuilders.standaloneSetup(this.usuarioService).build();

        this.usuario = new Usuario();
        this.usuario.setId(1L);
        this.usuario.setNome("John");
        this.usuario.setData_nascimento(new Date());

        this.usuarios = new ArrayList<>();
        this.usuarios.add(this.usuario);


    }

    @Test
    public void getOneSuccess(){
        long id = 1L;

        when(this.usuarioRepository.getOne(id)).thenReturn(this.usuario);

        Usuario found = this.usuarioService.getById(id);

        assertNotNull(found);
        assertEquals(id, found.getId());

        verify(this.usuarioRepository, times(1)).getOne(id);

        verifyNoMoreInteractions(this.usuarioRepository);
    }

}
