package com.neppo.estagio.view.services.implementation;

import com.neppo.estagio.core.models.Usuario;
import com.neppo.estagio.core.repositories.UsuarioRepository;
import com.neppo.estagio.view.services.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UsuarioServiceImpl implements UsuarioService {

    private UsuarioRepository usuarioRepository;

    @Autowired
    public UsuarioServiceImpl(UsuarioRepository usuarioRepository) {
        this.usuarioRepository = usuarioRepository;
    }

    @Override
    public Usuario getById(Long id) {
        return usuarioRepository.getOne(id);
    }

    @Override
    public List<Usuario> getAll() {
        return usuarioRepository.findAll();
    }

    @Override
    public Usuario save(Usuario person) {
        return usuarioRepository.save(person);
    }

    @Override
    public List<Usuario> getByName(String name) {
        return usuarioRepository.findByNome(name);
    }

    @Override
    public List<Usuario> getByNameContaining(String name) {
        return usuarioRepository.findByNomeContainsIgnoreCase(name);
    }
}
